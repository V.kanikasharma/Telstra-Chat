package com.ing.INGCHAT;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class IngChatApplication {

	public static void main(String[] args) {
		SpringApplication.run(IngChatApplication.class, args);
	}
}
