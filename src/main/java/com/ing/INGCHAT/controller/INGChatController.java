package com.ing.INGCHAT.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.ing.INGCHAT.dao.GroupRepository;
import com.ing.INGCHAT.dao.UserContactRepository;
import com.ing.INGCHAT.dao.UserMessageRepository;
import com.ing.INGCHAT.dao.UserServiceRepository;
import com.ing.INGCHAT.model.Group;
import com.ing.INGCHAT.model.User;
import com.ing.INGCHAT.model.UserContact;
import com.ing.INGCHAT.model.UserDTO;
import com.ing.INGCHAT.model.UserMessage;

@RestController
public class INGChatController {
	
	@Autowired
	@Qualifier("UserRepository")
	private UserServiceRepository userRepo;
	
	@Autowired
	@Qualifier("GroupRepository")
	private GroupRepository groupRepo;
	
	
	@PostMapping("/users/register")
	@CrossOrigin
	@ResponseStatus(value=HttpStatus.OK,reason="Successfully Registered")
	public ResponseEntity<?> createUser(@RequestBody User user,UriComponentsBuilder ucBuilder) {
		userRepo.save(user);          
		return new ResponseEntity<String>("Success", HttpStatus.CREATED);

	}
	
	
	@PostMapping("/users/login")
	@CrossOrigin
	public User getLoggedInUser(@RequestBody User user) {
		return userRepo.loginUser(user);
	}
	
    @PostMapping("/users/group")
    @CrossOrigin
    public ResponseEntity<Object> createGroup(@RequestBody Group group) {
           Group savedGroup = groupRepo.save(group);

           URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedGroup.getGroupId()).toUri();

           return ResponseEntity.created(location).build();
    }
    
    @GetMapping("/users/group/byName")
	@CrossOrigin
	public Group findGroupByName(@RequestParam String groupName) {
		return groupRepo.findByGroupName(groupName);
	}
      
    
    @Autowired
	private UserContactRepository ucRepo;

/**
	 * Method will search users by user name and company name.
	 * @param userName
	 * @param companyName
	 * 
	 */
	@GetMapping("/users/byName")
	@CrossOrigin
	Set<UserDTO> searchByUserName(@RequestParam String userName, @RequestParam String companyName){
		
		return convertDaoToDto(userRepo.findByUserName(userName, companyName));
	}



    @PostMapping("/users/contact/{id}")
    @CrossOrigin
    public Set<UserDTO> createUserContact(@PathVariable Long id, @RequestBody UserContact userContact) {
    	ucRepo.save(userContact);
    	
    	List<UserContact> ucList = ucRepo.findByPrimaryUserId(id);
    	List<User> userContacts = new ArrayList<User>();
    	for(UserContact uc : ucList){
    		userContacts.add(userRepo.findById(uc.getContactId()).get());
    	}
    	
    	return convertDaoToDto(userContacts);
    }
    
    
    /*
     * This method takes List<User> object and converts it into Set<SearchUserDTO>
     */
    private Set<UserDTO> convertDaoToDto(List<User> userDao){
    	Set<UserDTO> userDtoList = new HashSet<UserDTO>();
		UserDTO dto = new UserDTO();
		for(User user: userDao){
			dto.setUserId(user.getUserId());
			dto.setUserName(user.getUserName());
			userDtoList.add(dto);
			dto = new UserDTO();
		}
		
		return userDtoList;
    }

    @Autowired
    @Qualifier("UMRepository")
    private UserMessageRepository umRepo;

    @PostMapping("/users/message")
        @CrossOrigin
        @ResponseStatus(value=HttpStatus.OK,reason="Message Stored")
        public ResponseEntity<?> createUserMessage(@RequestBody UserMessage message) {
        	umRepo.save(message);
        	
        	return new ResponseEntity<String>("Success", HttpStatus.CREATED);
        }
        
        @GetMapping("/users/message")
        @CrossOrigin
        public List<UserMessage> getUserMessage(@RequestParam Long senderId, @RequestParam Long receiverId) {
        	
        	List<UserMessage> result = umRepo.findMessages(senderId, receiverId); 
        	return result;
        }
        
        
        @DeleteMapping("/users/message/{id}")
        @ResponseStatus(value=HttpStatus.OK,reason="Successfully Deleted")
        @CrossOrigin
        public ResponseEntity<?> deleteUserMessage(@PathVariable Long id) {
        	
        	umRepo.deleteById(id);
        	return new ResponseEntity<String>("Success", HttpStatus.CREATED);
        }
        
        
	
}
