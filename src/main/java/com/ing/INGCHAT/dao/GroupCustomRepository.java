package com.ing.INGCHAT.dao;

import org.springframework.stereotype.Repository;

import com.ing.INGCHAT.model.Group;

@Repository
public interface GroupCustomRepository {
	
	Group findByGroupName(String groupName);
 
}
