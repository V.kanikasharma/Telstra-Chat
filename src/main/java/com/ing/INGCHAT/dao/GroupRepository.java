package com.ing.INGCHAT.dao;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ing.INGCHAT.model.Group;

@Repository
@Qualifier("GroupRepository")
public interface GroupRepository extends JpaRepository<Group, Long>, GroupCustomRepository {
}
