package com.ing.INGCHAT.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ing.INGCHAT.model.UserContact;

@Repository
public interface UserContactRepository extends JpaRepository<UserContact, Long>{
	
	List<UserContact> findByPrimaryUserId(Long primaryUserId);
  
	
}
