package com.ing.INGCHAT.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ing.INGCHAT.model.UserMessage;

@Repository
public interface UserMessageCustomRepository {
	
	List<UserMessage> findMessages(Long senderId, Long receiverId);
 
}
