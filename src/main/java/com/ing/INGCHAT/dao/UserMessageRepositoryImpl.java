package com.ing.INGCHAT.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ing.INGCHAT.model.UserMessage;


public class UserMessageRepositoryImpl implements UserMessageCustomRepository {
	
	@PersistenceContext
	EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<UserMessage> findMessages(Long senderId, Long receiverId) {
		Query query = entityManager.createNativeQuery("select u.* from User_Message as u where (u.Sender_Id =? or u.Sender_Id=?) and (u.Receiver_Id =? or u.Receiver_Id=?)",UserMessage.class);
		query.setParameter(1, senderId);
		query.setParameter(2,receiverId);
		query.setParameter(3, senderId);
		query.setParameter(4,receiverId);
		List<UserMessage> result = ((List<UserMessage>)query.getResultList());
		return result;
	}
 
	
}
