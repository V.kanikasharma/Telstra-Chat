package com.ing.INGCHAT.dao;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ing.INGCHAT.model.User;

@Repository
@Qualifier("UserRepository")
public interface UserServiceRepository extends JpaRepository<User, Long>, UserServices{
	
}
