package com.ing.INGCHAT.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ing.INGCHAT.model.User;



public class UserServiceRepositoryImpl implements UserServices {
	
	@PersistenceContext
	EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findByUserName(String userName, String companyName) {
		Query query = entityManager.createNativeQuery("select u.* from User as u where u.User_Name like ? and u.Company_Name=?",User.class);
		query.setParameter(1, "%"+userName+"%");
		query.setParameter(2, companyName);
		List<User> result = ((List<User>)query.getResultList());
		return result;
		
	}

	@Override
	public User loginUser(User user) {
		
		String userName= user.getUserName();
		String password= user.getPassword();
		
		Query query = entityManager.createNativeQuery("select * from user where user_name=? and password =?",User.class);
		query.setParameter(1, userName);
		query.setParameter(2, password);
		User result = (User) query.getSingleResult();
		
		if(result==null) {
			
			return null;
		}
		
		return result;
	}

	
	
}
