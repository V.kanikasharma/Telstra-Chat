package com.ing.INGCHAT.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ing.INGCHAT.model.User;

@Repository
public interface UserServices {
   
	List<User> findByUserName(String userName, String companyName);
	
	User loginUser(User user);
	
}
