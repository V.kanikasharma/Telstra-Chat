package com.ing.INGCHAT.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="USER_CONTACT")
public class UserContact {
	
	public UserContact(){}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long primaryUserId;
	
	private Long contactId;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the primaryUserId
	 */
	public Long getPrimaryUserId() {
		return primaryUserId;
	}

	/**
	 * @param primaryUserId the primaryUserId to set
	 */
	public void setPrimaryUserId(Long primaryUserId) {
		this.primaryUserId = primaryUserId;
	}

	/**
	 * @return the contactId
	 */
	public Long getContactId() {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	
}
