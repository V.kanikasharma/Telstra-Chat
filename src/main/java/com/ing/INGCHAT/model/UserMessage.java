package com.ing.INGCHAT.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="USER_MESSAGE")
public class UserMessage {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long messageId;
	
	private long senderId;
	private long receiverId;
	private String message;
	private Date messageTs;
	/**
	 * @return the messageId
	 */
	public long getMessageId() {
		return messageId;
	}
	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	/**
	 * @return the senderId
	 */
	public long getSenderId() {
		return senderId;
	}
	/**
	 * @param senderId the senderId to set
	 */
	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}
	/**
	 * @return the receiverId
	 */
	public long getReceiverId() {
		return receiverId;
	}
	/**
	 * @param receiverId the receiverId to set
	 */
	public void setReceiverId(long receiverId) {
		this.receiverId = receiverId;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the messageTs
	 */
	public Date getMessageTs() {
		return messageTs;
	}
	/**
	 * @param messageTs the messageTs to set
	 */
	public void setMessageTs(Date messageTs) {
		this.messageTs = messageTs;
	}
	
	
	

}
